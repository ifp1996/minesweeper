package Minesweeper;
import java.util.Random;
import java.util.Scanner;

import Core.Board;
import Core.Field;
import Core.Window;
public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
	
		Random rnd = new Random();
		Scanner sc = new Scanner(System.in);
		
		int r = 8;
		int c = 8;
		int m = 20;
		int i = 0;
		int rndc = 0;
		int rndr = 0;
		int x;
		int y;
		while(true) {
			System.out.print("Select an option:\n");
			System.out.print("1. Display Help\n");
			System.out.print("2. Settings\n");
			System.out.print("3. Play\n");
			System.out.print("4. Check Rankings\n");
			System.out.print("0. Exit\n");
			String key = sc.next();
			switch (key) {
			case "1":
				System.out.print("The game rules are the following:\n");
				System.out.print("Each attempt, click on a square to reveal it.\n");
				System.out.print("If a mine is revealed, the game is lost.\n");
				System.out.print("If no mine is revealed, the rest of the boxes that don't have any nearby will reveal alongside with the selected one");
				System.out.print("If any mine is nearby the selected square, it'll reveal specifically how many there are around it");
				System.out.print("The game ends when you either lose by a mine explosion or by completing the field without detonating any of the set mines");
				break;
			
			case "2":
				System.out.print("Specify the number of rows you want to set in-game:");
				r = sc.nextInt();
				if(r < 8) {
					throw new IllegalArgumentException("Rows cannot be under 8 squares.");
				}
				if(r > 16) {
					throw new IllegalArgumentException("Rows cannot be above 16 squares.");
				}
				System.out.print("Specify the number of columns you want to set in-game:");
				c = sc.nextInt();
				if(c < 8) {
					throw new IllegalArgumentException("Columns cannot be under 8 squares.");
				}
				if(c > 16) {
					throw new IllegalArgumentException("Columns cannot be above 8 squares.");
				}
				System.out.print("Specify the number of mines you want to set in-game:");
				m = sc.nextInt();
				if(m <= 0) {
					System.out.print("You can't put no mines in a game that there are supposed to be in.");
				}
				if(m == r*c) {
					System.out.print("You can't put the whole field plenty of mines.");
				}
				break;
			
			case "3":
				boolean minepl = false;
				int mines[][] = new int[c][r];
				
				while(!minepl) {
					rndc = rnd.nextInt(c);
					rndr = rnd.nextInt(r);
					if(mines[rndc][rndr] != 1) {
						mines[rndc][rndr] = 1;
						i++;
					}
					if(i == m) {
						minepl = true;
					}
				}
				
				
				/*for(int k = 0; k < mines.length; k++) {
						for(int j = 0; j < mines[0].length; j++) {
	
							System.out.print(mines[k][j]);
							
						}
						System.out.println();
					} System.out.println("--------");*/
					
				//Mine placement display using 'for'.
				
				int field[][] = new int[c][r];
				
				for(int j = 0; j < field.length; j++) {
					for(int k = 0; k < field[0].length; k++) {
						field[j][k] = 9; // Defines value as 'not revealed'.
					}
				}
				
				boolean lost = false;
				boolean won = false;
				
				Board b = new Board();
				Window w = new Window(b);
				w.setTitle("Minesweeper");
				String[] txt = { "", "1", "2", "3", "4", "5", "6", "7", "8", "?", "*" };
				int[] txtcolors = { 0x000000, 0x2e41f0, 0x10662d, 0xd93523, 0x281780, 0x632316, 0x1c8fa3, 0x111212, 0x99a1a1, 0xffffff };
				b.setColorbackground(0xcccac6);
				b.setActborder(true); //Cell border boolean.
				b.setText(txt);
				b.setColortext(txtcolors);
				
				b.draw(field, 't');
				
				while(!lost && !won) {
					
						try {
							Thread.sleep(50);  //This function prevents the Mouse Input from collapsing.
						} catch (InterruptedException e) {} 
						
						y = b.getCurrentMouseCol();
						x = b.getCurrentMouseRow();
						if (y != -1 && x != -1) {
							
							if(mines[y][x] == 1) {
								field[y][x] = 10;
								System.out.println("GAME OVER");
								lost = true;
							}else {
								
							}
							
						}


						
					
				}
				
				break;
			
			case "4":
				//I'll think of something about the ranking system.
				break;
				
			case "0":
				System.exit(0);
			default:
				throw new IllegalArgumentException("You must select a valid option.");
			}
		}
	}

}
